package main

import (
	"sort"
	"strconv"
)

func ReturnInt() int {
	return 1
}

func ReturnFloat() float32 {
	return 1.1
}

func ReturnIntArray() [3]int {
	return [3]int{1, 3, 4}
}

func ReturnIntSlice() []int {
	return []int{1, 2, 3}
}

func IntSliceToString(intSlice []int) string {
	var result string
	for _, num := range intSlice {
		result += strconv.Itoa(num)
	}
	return result
}

func MergeSlices(floatSlice []float32, intSlice []int32) []int {
	var result []int
	for _, floatNum := range floatSlice {
		result = append(result, int(floatNum))
	}
	for _, intNum := range intSlice {
		result = append(result, int(intNum))
	}
	return result
}

func GetMapValuesSortedByKey(source map[int]string) []string {
	var result []string
	var indexSlice []int

	for key := range source {
		indexSlice = append(indexSlice, key)
	}

	sort.Ints(indexSlice)
	for _, mapKey := range indexSlice {
		result = append(result, source[mapKey])
	}

	return result
}
